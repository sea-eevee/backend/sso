package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/sso/api_sso"
	"gitlab.com/sea-eevee/backend/sso/uc_sso"
	"net/http"
)

func NewRouter(sso uc_sso.Contract) *mux.Router {
	root := mux.NewRouter()

	root.Methods(http.MethodGet).Path("/ping").HandlerFunc(ping)
	root.
		Methods(http.MethodPost).
		Path("/login").
		Handler(api_sso.Login(sso.Login))

	root.
		Methods(http.MethodPost).
		Path("/register").
		Handler(api_sso.Register(sso.Register))

	return root
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong from sso service")
}

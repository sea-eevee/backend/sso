package uc_sso

import "gitlab.com/sea-eevee/backend/sso/pkg/constant"

func isParamRoleExists(role string) bool {
	if role == constant.RoleAdmin || role == constant.RoleMerchant || role == constant.RoleCustomer {
		return true
	}
	return false
}

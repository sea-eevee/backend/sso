package uc_sso

import (
	"gitlab.com/sea-eevee/backend/sso/pkg/errs"
	"golang.org/x/crypto/bcrypt"
)

type LoginParam struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

type LoginUCFunc func(param *LoginParam) (*AuthResponse, error)

func (u ucSSO) Login(param *LoginParam) (*AuthResponse, error) {
	if !isParamRoleExists(param.Role) {
		return nil, errs.ErrRoleNotExists
	}

	uid, hashedPassword, err := u.repoSSO.GetUIDPassByUsernameAndRole(param.Username, param.Role)
	if err != nil {
		return nil, err
	}
	err = bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(param.Password))
	if err != nil {
		return nil, errs.ErrAuth
	}

	ts, err := u.tokenizer.CreateToken(param.Role, uid)
	if err != nil {
		return nil, err
	}

	return &AuthResponse{
		AccessToken: ts,
		UserID:      uid,
		Username:    param.Username,
	}, nil
}

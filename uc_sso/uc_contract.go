package uc_sso

import (
	"gitlab.com/sea-eevee/backend/common/tokenizer"
	"gitlab.com/sea-eevee/backend/sso/repo/repo_customer"
	"gitlab.com/sea-eevee/backend/sso/repo/repo_sso"
)

type AuthResponse struct {
	AccessToken string `json:"access_token"`
	UserID      uint64 `json:"user_id"`
	Username    string `json:"username"`
}

type Contract interface {
	Register(param *RegisterParam) (*AuthResponse, error)
	Login(param *LoginParam) (*AuthResponse, error)
}

type ucSSO struct {
	repoSSO      repo_sso.Contract
	repoCustomer repo_customer.Contract
	tokenizer    tokenizer.Contract
}

func NewUCSSO(repoSSO repo_sso.Contract, repoCustomer repo_customer.Contract, tokenizer tokenizer.Contract) Contract {
	return &ucSSO{
		repoSSO:      repoSSO,
		repoCustomer: repoCustomer,
		tokenizer:    tokenizer,
	}
}

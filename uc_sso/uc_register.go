package uc_sso

import (
	"gitlab.com/sea-eevee/backend/sso/pkg/constant"
	"gitlab.com/sea-eevee/backend/sso/pkg/errs"
	"golang.org/x/crypto/bcrypt"
)

type RegisterParam struct {
	Username   string `json:"username"`
	Password   string `json:"password"`
	Role       string `json:"role"`
	Email      string `json:"email"`
	AdminToken string `json:"admin_token"`
}

type RegisterUCFunc func(param *RegisterParam) (*AuthResponse, error)

func (u ucSSO) Register(param *RegisterParam) (*AuthResponse, error) {
	if !isParamRoleExists(param.Role) {
		return nil, errs.ErrRoleNotExists
	}

	uid, _, err := u.repoSSO.GetUIDPassByUsernameAndRole(param.Username, param.Role)
	if err != nil || uid != 0 {
		return nil, errs.ErrUsernameExists
	}

	bytes, err := bcrypt.GenerateFromPassword([]byte(param.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	hashedPassword := string(bytes)

	var insertedID uint64
	if param.Role == constant.RoleAdmin {
		existToken, err := u.repoSSO.IsAdminTokenExists(param.AdminToken)
		if err != nil || !existToken {
			return nil, errs.ErrAdminTokenNotExists
		}
		insertedID, err = u.repoSSO.InsertAdmin(param.Username, hashedPassword)
		if err != nil {
			return nil, err
		}
	} else { // "merchant" or "customer"
		insertedID, err = u.repoSSO.InsertMerchantOrCustomer(param.Username, hashedPassword, param.Email, param.Role)
		if err != nil {
			return nil, err
		}
		if param.Role == constant.RoleCustomer {
			err := u.repoCustomer.ProfileCreate(insertedID)
			if err != nil {
				return nil, err
			}
		}

	}

	accessToken, err := u.tokenizer.CreateToken(constant.RoleAdmin, insertedID)
	if err != nil {
		return nil, err
	}
	return &AuthResponse{
		AccessToken: accessToken,
		UserID:      insertedID,
		Username:    param.Username,
	}, nil
}

package uc_sso

import (
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sea-eevee/backend/common/tokenizer"
	"gitlab.com/sea-eevee/backend/sso/pkg/constant"
	"gitlab.com/sea-eevee/backend/sso/pkg/errs"
	mock_repo_customer "gitlab.com/sea-eevee/backend/sso/repo/repo_customer/mock"
	mock_repo_sso "gitlab.com/sea-eevee/backend/sso/repo/repo_sso/mock"
	"testing"
)

func Test_ucSSO_Register(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepoSSO := mock_repo_sso.NewMockContract(ctrl)
	mockRepoCustomer := mock_repo_customer.NewMockContract(ctrl)
	tz := tokenizer.NewTokenizer("secret", 10000)
	u := NewUCSSO(mockRepoSSO, mockRepoCustomer, tz)

	sampleUsername := "usernameku"
	samplePassword := "passwordku"
	//hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(samplePassword), 0)

	var tts = []struct {
		caseName     string
		params       LoginParam
		expectations func()
		results      func(resp *AuthResponse, err error)
	}{
		{
			caseName: "when everything is normal should not return error",
			params:   LoginParam{Username: sampleUsername, Password: samplePassword, Role: constant.RoleCustomer},
			expectations: func() {
				mockRepoSSO.EXPECT().GetUIDPassByUsernameAndRole(gomock.Any(), constant.RoleCustomer).Return(uint64(0), "", nil)
			},
			results: func(resp *AuthResponse, err error) {
				assert.Equal(t, errs.ErrAuth, err)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		tt.expectations()

		tt.results(u.Login(&tt.params))
	}
}

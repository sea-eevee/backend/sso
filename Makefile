.PHONY : network-create db-build db-run db-stop app-build app-run app-stop app-clean

network:
	docker network inspect compfest-marketplace >/dev/null 2>&1 || \
        docker network create --driver bridge compfest-marketplace

db-reload:
	docker container stop compfest-marketplace-service-sso-db || true
	docker build -t compfest-marketplace-service-sso-db-img -f Dockerfile.postgres .
	docker run -d --rm --name compfest-marketplace-service-sso-db \
        --network=compfest-marketplace \
        -p 14000:5432 \
        -v compfest-marketplace-sso-vol:/var/lib/postgresql/data \
        -e POSTGRES_USER=pgku \
        -e POSTGRES_PASSWORD=pgku \
        -e POSTGRES_DB=sso \
        compfest-marketplace-service-sso-db-img

app-build:
	docker build -t compfest-marketplace-service-sso -f Dockerfile.app .

app-run:
	docker run -d --rm --name compfest-marketplace-service-sso \
	--network=compfest-marketplace \
	-p 9000:9000 \
	compfest-marketplace-service-sso

app-stop:
	docker stop compfest-marketplace-service-sso || true

app-reload:
	make app-stop
	make app-build
	make app-run

app-clean:
	docker rmi -f compfest-marketplace-service-sso

full-reload:
	make db-reload
	make app-reload
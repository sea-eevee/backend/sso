package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/common/config"
	"gitlab.com/sea-eevee/backend/common/store"
	"gitlab.com/sea-eevee/backend/common/tokenizer"
	"gitlab.com/sea-eevee/backend/sso/repo/repo_customer"
	"gitlab.com/sea-eevee/backend/sso/repo/repo_sso"
	"gitlab.com/sea-eevee/backend/sso/uc_sso"
	"log"
	"net/http"
	"os"
)

func main() {
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	cfg := config.InitAppConfig(path)

	tz := tokenizer.NewTokenizer(cfg.Secret, int64(cfg.JwtAccessExpires))

	pgConn := store.DBConn{
		DBHost: "compfest-marketplace-service-sso-db",
		DBPort: cfg.DBPort,
		//DBHost: "localhost",
		//DBPort: 14000,
		DBUser: cfg.DBUser,
		DBPass: cfg.DBPass,
		DBName: "sso",
	}
	rp, err := repo_sso.NewRepo(pgConn)
	if err != nil {
		panic(err)
	}
	rpCust, err := repo_customer.NewRepoCustomer(store.DBConn{
		DBHost: "compfest-marketplace-service-customer-db",
		DBPort: cfg.DBPort,
		//DBHost: "localhost",
		//DBPort: 13300,
		DBUser: cfg.DBUser,
		DBPass: cfg.DBPass,
		DBName: "customer",
	})
	if err != nil {
		panic(err)
	}
	ucSSO := uc_sso.NewUCSSO(rp, rpCust, tz)

	r := NewRouter(ucSSO)
	r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		tpl, err1 := route.GetPathTemplate()
		met, err2 := route.GetMethods()
		fmt.Println(tpl, err1, met, err2)
		return nil
	})

	err = http.ListenAndServe(":9000", r)
	if err != nil {
		panic(err)
	}
}

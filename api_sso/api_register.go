package api_sso

import (
	"encoding/json"
	"gitlab.com/sea-eevee/backend/sso/pkg/errs"
	"gitlab.com/sea-eevee/backend/sso/pkg/responder"
	"gitlab.com/sea-eevee/backend/sso/uc_sso"
	"net/http"
)

func Register(ucFunc uc_sso.RegisterUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Body == nil {
			responder.ResponseError(w, errs.ErrBadRequest)
			return
		}

		param := new(uc_sso.RegisterParam)
		err := json.NewDecoder(r.Body).Decode(param)
		if err != nil {
			responder.ResponseError(w, errs.ErrBadRequest)
			return
		}

		resp, err := ucFunc(&uc_sso.RegisterParam{
			Username:   param.Username,
			Password:   param.Password,
			AdminToken: param.AdminToken,
			Role:       param.Role,
			Email:      param.Email,
		})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}

package api_sso

import (
	"bytes"
	"encoding/json"
)

const (
	URLKeyRole = "role"
)

func paramToBody(p interface{}) *bytes.Buffer {
	sampleBody, _ := json.Marshal(p)
	return bytes.NewBuffer(sampleBody)
}

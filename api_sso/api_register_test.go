package api_sso

import (
	"errors"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sea-eevee/backend/sso/pkg/constant"
	"gitlab.com/sea-eevee/backend/sso/uc_sso"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRegister(t *testing.T) {

	sampleError := errors.New("any error")
	const (
		sampleUsername = "usernameku"
		samplePassword = "passwordku"
		sampleEmail    = "emailku@gmail.com"

		caseCustOK = "customer ok"
	)

	pt := map[string]uc_sso.RegisterParam{
		caseCustOK: {
			Username: sampleUsername,
			Password: samplePassword,
			Email:    sampleEmail,
			Role:     constant.RoleAdmin,
		},
	}

	var tts = []struct {
		caseName    string
		handlerFunc uc_sso.RegisterUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName:    "when body param is nil return 400 Bad Request",
			handlerFunc: nil,
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodPost, "/", nil)
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
			},
		},
		{
			caseName:    "when body param not is right return 400 Bad Request",
			handlerFunc: nil,
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodPost, "/", paramToBody("wrong body"))
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
			},
		},
		{
			caseName: "when body param is right and handlerFunc not return any error should return 200 OK",
			handlerFunc: func(param *uc_sso.RegisterParam) (*uc_sso.AuthResponse, error) {
				assert.Equal(t, pt[caseCustOK].Username, param.Username)
				assert.Equal(t, pt[caseCustOK].Password, param.Password)
				assert.Equal(t, pt[caseCustOK].Email, param.Email)
				assert.Equal(t, pt[caseCustOK].Role, param.Role)

				return &uc_sso.AuthResponse{}, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodPost, "/", paramToBody(pt[caseCustOK]))
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
		{
			caseName: "when body param is right, but handlerFunc return some error should not return 200 OK",
			handlerFunc: func(param *uc_sso.RegisterParam) (*uc_sso.AuthResponse, error) {
				assert.Equal(t, pt[caseCustOK].Username, param.Username)
				assert.Equal(t, pt[caseCustOK].Password, param.Password)
				assert.Equal(t, pt[caseCustOK].Email, param.Email)
				assert.Equal(t, pt[caseCustOK].Role, param.Role)

				return &uc_sso.AuthResponse{}, sampleError
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodPost, "/", paramToBody(pt[caseCustOK]))
				return r
			},
			result: func(resp *http.Response) {
				assert.NotEqual(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/").Handler(Register(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()

		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}

package errs

import "errors"

var (
	ErrAuth                = errors.New("err auth")
	ErrBadRequest          = errors.New("err bad request")
	ErrUsernameExists      = errors.New("err username exists")
	ErrAdminTokenNotExists = errors.New("err admin token not exists")
	ErrRoleNotExists       = errors.New("err role not exists")
)

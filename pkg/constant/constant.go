package constant

const (
	RoleAdmin    = "admin"
	RoleMerchant = "merchant"
	RoleCustomer = "customer"
)

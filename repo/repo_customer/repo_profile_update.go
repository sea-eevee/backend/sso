package repo_customer

func (r repoCustomer) ProfileCreate(customerID uint64) error {
	query := `INSERT INTO customer_profile(customer_id) VALUES ($1)`

	_, err := r.db.Exec(query, customerID)
	if err != nil {
		return err
	}
	return nil
}

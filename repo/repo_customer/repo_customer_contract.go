package repo_customer

import (
	"database/sql"
	_ "github.com/lib/pq"
	"gitlab.com/sea-eevee/backend/common/store"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_customer . Contract

type Contract interface {
	ProfileCreate(customerID uint64) error
}

type repoCustomer struct {
	db *sql.DB
}

func NewRepoCustomer(param store.DBConn) (Contract, error) {
	db, err := store.NewPostgres(param)
	if err != nil {
		return nil, err
	}
	return &repoCustomer{
		db: db,
	}, nil
}

package repo_sso

import (
	"database/sql"
	"gitlab.com/sea-eevee/backend/common/store"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_sso . Contract

type Contract interface {
	IsAdminTokenExists(token string) (bool, error)
	InsertAdmin(username, password string) (insertedID uint64, err error)
	InsertMerchantOrCustomer(username, password, email, role string) (insertedID uint64, err error)

	GetUIDPassByUsernameAndRole(username, role string) (uid uint64, hashedPassword string, err error)
}

type repoSSO struct {
	db *sql.DB
}

func NewRepo(param store.DBConn) (Contract, error) {
	db, err := store.NewPostgres(param)
	if err != nil {
		return nil, err
	}
	return &repoSSO{
		db: db,
	}, nil
}

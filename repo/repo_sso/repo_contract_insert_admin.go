package repo_sso

func (r repoSSO) InsertAdmin(username, password string) (insertedID uint64, err error) {
	query := "INSERT INTO admin(username, hash_password) VALUES ($1, $2) RETURNING id"
	err = r.db.QueryRow(query, username, password).Scan(&insertedID)
	if err != nil {
		return 0, err
	}
	return insertedID, err
}

func (r repoSSO) IsAdminTokenExists(token string) (bool, error) {
	query := "SELECT count(id) FROM admin_token WHERE token = $1 LIMIT 1"
	rows, err := r.db.Query(query, token)
	if err != nil {
		return false, err
	}

	var count int
	for rows.Next() {
		err := rows.Scan(&count)
		if err != nil {
			return false, nil
		}
		return count == 1, nil
	}
	return count == 1, nil
}

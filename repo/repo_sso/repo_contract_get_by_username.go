package repo_sso

import "fmt"

func (r repoSSO) GetUIDPassByUsernameAndRole(username, role string) (uid uint64, hashedPassword string, err error) {
	query := fmt.Sprintf("SELECT id, hash_password FROM %s WHERE username=$1;", role)

	rows, err := r.db.Query(query, username)
	if err != nil {
		return 0, "", err
	}
	for rows.Next() {
		err = rows.Scan(&uid, &hashedPassword)
		if err != nil {
			return 0, "", err
		}
	}
	return uid, hashedPassword, nil
}

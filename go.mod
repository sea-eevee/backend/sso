module gitlab.com/sea-eevee/backend/sso

go 1.15

require (
	github.com/golang/mock v1.4.3
	github.com/gorilla/handlers v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.8.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/sea-eevee/backend/common v0.0.0-20200902182313-47d857510720
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
)

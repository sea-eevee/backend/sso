CREATE TABLE customer (
  id BIGSERIAL PRIMARY KEY,
  username varchar(255) UNIQUE NOT NULL,
  hash_password varchar(255) NOT NULL,
  email varchar(255)
);
INSERT INTO customer(id, username, hash_password, email)VALUES (1, 'john.doe.customer', '$2a$10$Cg8cg2eow3LLPoDfQ1nDTurbknT3NTArCeeuXe8km0OIXlYWSz5Ly', 'john.doe.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email)VALUES (2, 'madara.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'madara.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email)VALUES (3, 'sasuke.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'sasuke.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email)VALUES (4, 'naruto.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'naruto.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email)VALUES (5, 'sanji.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'sanji.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email)VALUES (6, 'nami.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'nami.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email)VALUES (7, 'luffy.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'luffy.customer@gmail.com');
select setval('customer_id_seq'::regclass,8,false);

CREATE TABLE merchant (
  id BIGSERIAL PRIMARY KEY,
  username VARCHAR(255) UNIQUE NOT NULL,
  hash_password VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  admin_approval BOOLEAN
);
INSERT INTO merchant(id, username, hash_password, email)VALUES (1, 'john.doe.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'ferdian.ifkarsyah@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (2, 'samsung.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'samsung.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (3, 'apple.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'apple.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (4, 'xiaomi.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'xiaomi.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (5, 'adidas.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'adidas.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (6, 'nike.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'nike.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (7, 'gunung.agung.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'gunung.agung.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (8, 'gramedia.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'gramedia.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (9, 'lg.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'lg.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (10, 'panasonic.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'panasonic.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (11, 'toshiba.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'toshiba.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (12, 'acer.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'acer.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (13, 'asus.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'asus.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (14, 'wibu.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'wibu.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (15, 'kpoper.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'kpoper.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (16, 'tejo.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'tejo.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email)VALUES (17, 'yuning.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'yuning.merchant@gmail.com');
select setval('merchant_id_seq'::regclass,18,false);

CREATE TABLE admin (
  id BIGSERIAL PRIMARY KEY,
  username varchar(255) NOT NULL,
  hash_password varchar(255) NOT NULL
);
INSERT INTO admin(username, hash_password) VALUES ('john.doe.admin', '$2a$10$4jWZ.8fORKmMuO3KJzWESube42zZHSbBWq7YtdxEbQNSIoZtW.18C');
select setval('admin_id_seq'::regclass,2,false);

CREATE TABLE admin_token (
  id BIGSERIAL PRIMARY KEY,
  token varchar(255) NOT NULL,
  used boolean DEFAULT false
);
INSERT INTO admin_token(token) VALUES ('tokenku');
INSERT INTO admin_token(token) VALUES ('tokenmu');
INSERT INTO admin_token(token) VALUES ('tokendia');
select setval('admin_token_id_seq'::regclass,4,false);